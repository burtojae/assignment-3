﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleToggle : MonoBehaviour
{

    private PlayerController player;
    private ParticleSystem self;


    void Start()
    {
        player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
        self = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        if (player.charge == player.maxCharge && self.isEmitting == false)
        {
            self.Play();
        }
        else if (player.charge != player.maxCharge && self.isEmitting == true)
        {
            self.Stop();
        }
    }
}