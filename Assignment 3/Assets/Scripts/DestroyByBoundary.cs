﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByBoundary : MonoBehaviour {

    void OnTriggerExit(Collider other)
    {
        //It was possible for the player to barrel roll out of existance.
        if (other.tag != "Player")
        {
            Destroy(other.gameObject);
        }
    }
}
