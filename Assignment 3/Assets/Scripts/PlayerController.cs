﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}


public class PlayerController : MonoBehaviour {

    private Rigidbody _rb;
    private float nextFire;
    private bool willRollRight;
    private bool willRollLeft;
    private Vector3 roll;
    private bool laserFiring;


    public float speed;
    public Boundary boundary;
    public float tilt;
    public Vector3 shotOffset;
    public GameObject shot;
    public float fireRate;
    public float rollWait;
    public float rollDuration;
    public float rollSpeed;
    public int charge;
    public int maxCharge;
    public float laserDuration;
    public Vector3 laserOffset;
    public GameObject GiantLaser;



    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    IEnumerator WillRollRightReset()
    {
        yield return new WaitForSeconds(rollWait);
        willRollRight = false;
    }

    IEnumerator WillRollLeftReset()
    {
        yield return new WaitForSeconds(rollWait);
        willRollLeft = false;
    }

    IEnumerator DoRoll()
    {
        yield return new WaitForSeconds(rollDuration);
        roll = new Vector3(0f,0f,0f);
    }

    IEnumerator LaserOff()
    {
        yield return new WaitForSeconds(laserDuration);
        Destroy(GameObject.FindWithTag("GiantLaser"));
        laserFiring = false;
        speed *= 2;
    }

    private void Update()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFire && laserFiring == false)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, _rb.position + shotOffset, _rb.rotation);
        }

        if (Input.GetButton("Fire2") && charge == maxCharge)
        {
            Instantiate(GiantLaser, _rb.position + laserOffset, _rb.rotation);
            charge = 0;
            laserFiring = true;
            StartCoroutine(LaserOff());
            speed /= 2;
        }

        //Barrel roll. Was unresponsive because it was in Fixed Update. Don't put input in the physics update process.
        if (Input.GetKeyDown(KeyCode.D))
        {
            if (willRollRight == true)
            {
                roll = new Vector3(rollSpeed, 0f, 0f);
                willRollRight = false;
                StartCoroutine(DoRoll());
            }

            willRollRight = true;
            StartCoroutine(WillRollRightReset());
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            if (willRollLeft == true)
            {
                roll = new Vector3(-rollSpeed, 0f, 0f);
                willRollLeft = false;
                StartCoroutine(DoRoll());
            }

            willRollLeft = true;
            StartCoroutine(WillRollLeftReset());
        }

    }

    private void FixedUpdate()
    {
        float moveH = Input.GetAxis("Horizontal");
        float moveV = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveH, 0f, moveV);
        _rb.velocity = (movement + roll) * speed;

        _rb.position = new Vector3(Mathf.Clamp(_rb.position.x, boundary.xMin, boundary.xMax), 0f, Mathf.Clamp(_rb.position.z, boundary.zMin, boundary.zMax));

        if (Mathf.Abs(roll.x) == 0)
        {
            _rb.rotation = Quaternion.Euler(0f, 0f, _rb.velocity.x * -tilt);
        }
        else
        {
            transform.Rotate(Vector3.back, (rollSpeed * 1000) * Time.deltaTime);
        }
        

    }
}
