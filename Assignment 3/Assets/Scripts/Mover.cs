﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {

    public float speed;

    private Rigidbody _rb;

	void Start () {
        _rb = GetComponent<Rigidbody>();
        _rb.velocity = transform.forward * speed;
	}

    private void FixedUpdate()
    {
        //Ensures that asteroids which bump into others can't stay still.
        if (_rb.velocity.z > transform.forward.z * speed)
        {
            _rb.velocity = transform.forward * speed;
        }
    }

}
