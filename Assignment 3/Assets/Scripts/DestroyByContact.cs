﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {

    public GameObject explosion;
    public GameObject playerExplosion;
    public GameObject energyOrb;
    public float scoreValue;
    public float orbChance;

    private GameController gameController;
    private RandomScale randomScale;

    void Start()
    {
        randomScale = GetComponent<RandomScale>();

        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }

        if (gameControllerObject == null)
        {
            Debug.Log("Cannot find Game Controller Script.");
        }

        scoreValue *= randomScale.health;
    }

    private void OnTriggerEnter(Collider other)
    {
        if ((randomScale.health <= 1 && other.tag == "Laser") || other.tag == "GiantLaser")
        {
            Destroy(gameObject);
            Instantiate(explosion, transform.position, transform.rotation);
            gameController.AddScore(scoreValue);
            if (Random.value <= orbChance)
            {
                Instantiate(energyOrb, transform.position, new Quaternion(0f, 0f, 0f, 0f));
            }
        }
        else if (other.tag == "Laser")
        {
            explosion.GetComponent<AudioSource>().volume = 0.3f;
            Instantiate(explosion, transform.position, transform.rotation);
            explosion.GetComponent<AudioSource>().volume = 1f;
            randomScale.health -= 1;
        }

        if (other.tag == "Boundary" || other.tag == "Item")
        {
            return;
        }
        
        if (other.tag == "Player") {
            Instantiate(explosion, transform.position, transform.rotation);
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            gameController.GameOver();
            Destroy(GameObject.FindWithTag("GiantLaser"));
        }

        if (other.tag != "GiantLaser")
        {
            Destroy(other.gameObject);
        }
    }
}
