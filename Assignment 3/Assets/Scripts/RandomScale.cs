﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomScale : MonoBehaviour {

    public float maxScale;
    public float minScale;
    public float health;
    private float newScale;
    private Mover mover;
    

	void Start () {
        mover = GetComponent<Mover>();
        newScale = Random.Range(minScale, maxScale);
        transform.localScale = new Vector3(newScale, newScale, newScale);
        health = Mathf.Round(newScale);
        mover.speed /= newScale / 2;
    }
}
