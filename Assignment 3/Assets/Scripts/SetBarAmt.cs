﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetBarAmt : MonoBehaviour {

    private GameObject playerObj;
    private PlayerController player;
    public Slider slider;


	// Use this for initialization
	void Start () {
        playerObj = GameObject.FindWithTag("Player");
        player = playerObj.GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
        slider.value = player.charge;
	}
}
