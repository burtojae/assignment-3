﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public GameObject hazard;
    public Vector3 spawnValues;
    public int hazardCount;
    public float hazardMaxScaleD;
    public float hazardMinScaleD;
    public float spawnWait;
    public float startWait;
    public float waveWait;
    public int gameScale;
    public float scaleAmt;

    public GUIText scoreText;
    public GUIText restartText;
    public GUIText gameOverText;

    private bool restart;
    private bool gameOver;

    private float score;
    private int wavesUntilScale;

    

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true) {
            if (gameOver)
            {
                break;
            }

            for (int i = 0; i < hazardCount; i++)
            {
                Vector3 spawnPos = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRot = Quaternion.identity;
                Instantiate(hazard, spawnPos, spawnRot);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);
            wavesUntilScale -= 1;
        }
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }

    public void GameOver()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
        restartText.text = "Press 'R' for Restart";
        restart = true;
    }

    public void AddScore(float newScore)
    {
        score += newScore;
        UpdateScore();
    }

    void Start () {
        gameOver = false;
        restart = false;
        gameOverText.text = "";
        restartText.text = "";
        score = 0;
        wavesUntilScale = 1;
        hazard.GetComponent<RandomScale>().maxScale = hazardMaxScaleD;
        hazard.GetComponent<RandomScale>().minScale = hazardMinScaleD;
        UpdateScore();
        StartCoroutine(SpawnWaves());
	}

    void Update()
    {
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene("Main"); //SceneManager.LoadScene(SceneManager.GetActiveScene().name); better solution in case of multiple scenes.
            }
        }

        if (wavesUntilScale == 0)
        {
            wavesUntilScale = 1;
            gameScale += 1;
            if (hazard.GetComponent<RandomScale>().maxScale > 3)
            { 
            hazard.GetComponent<RandomScale>().maxScale -= scaleAmt * 2;
            }

            if (hazard.GetComponent<RandomScale>().minScale > 1)
            {
                hazard.GetComponent<RandomScale>().minScale -= scaleAmt;
            }
            //Maybe make the spawn wait a range that decreases so there's a random element to the spawning
            //pattern. Might make it more interesting.
            if (spawnWait > 0.3f)
            {
                spawnWait -= scaleAmt;
            }
            else
            {
                spawnWait = 0.3f;
            }

            hazardCount += Mathf.RoundToInt(scaleAmt * 10f);
        }
    }


}
