﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectOrb : MonoBehaviour
{
    private PlayerController player;

    public int chargeAmt;
    
    void Start()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        player = other.GetComponent<PlayerController>();

        if (other.tag == "Player" && player.charge < 10)
        {
            Debug.Log("ENERGY");
            player.charge += chargeAmt;
            Destroy(gameObject);
        }
    }
}
